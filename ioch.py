#!/usr/bin/python

import os
import csv
import json
import urllib
import argparse
from time import sleep
from collections import Counter

parser = argparse.ArgumentParser(description='IOC Hunter (ioch) is a script that retrieves IOCs from VT based on exported PAN FW Traffic logs.')
parser.add_argument('-l','--log', help='path to traffic_logs.csv file that is to be parsed',required=True,type=str)
parser.add_argument('-k','--key',help='Specify your free public API key you already obtained from VirusTotal.',type=str)
parser.add_argument('-f','--frm',help='Query VirusTotal for Source or Destination IPs from log file.',required=True,choices=['src','dst'])
parser.add_argument('-r','--records', help='number of records for which you wish to retrieve IOCs.',required=False,type=int)
parser.add_argument('-s','--show', help='Do not lookup in VT, just print IPs found from the logs and sorted for review.',required=False,action='store_true')
args = parser.parse_args()

input_file  = args.log
api_key     = args.key
vt_url      = 'https://www.virustotal.com/vtapi/v2/ip-address/report'
addresses   = []
if args.frm == "src":
    addr_type = "Source address"
elif args.frm == "dst":
    addr_type = "Destination address"

if "APIKEY_VT" in os.environ:
    api_key = os.environ["APIKEY_VT"]
elif args.key is set:
    api_key = args.key
else:
    print("Missing API key in user environment or from CLI arguments!")
    exit()

# check if IP belongs to valid public space
def public_ip(ip):
    octets = ip.split('.')
    if octets[0] == "10":
        return False
    elif (octets[0] == "172") & ((octets[1] > "15") & (octets[1]<"32")):
        return False
    elif ((octets[0] == "192") & (octets[1] == "168")):
        return False
    else:
        return True

# query VT 
# returns: dict containing response
def get_ip_info(ip_address):
    parameters = {'ip': ip_address, 'apikey': api_key }
    vt_response = urllib.urlopen('%s?%s' % (vt_url, urllib.urlencode(parameters))).read()
    vt_response_dict = json.loads(vt_response)
    return vt_response_dict

# format VT results
def parse_vt_results(results):
    # get empty dict to gracefully fill in the table
    res = dict(asn="--",pdns="--",s_callbck="none",s_srcd="none",urls="none")

    if 'as_owner' in results:
        res['asn'] = results['as_owner'].__getslice__(0, 29).encode('utf-8')

    if 'resolutions' in results:
        res['pdns'] = str(len(results['resolutions']))

    if 'detected_communicating_samples' in results:
        if bool(results['detected_communicating_samples']):
            res['s_callbck'] = str(len(results['detected_communicating_samples'])) + " " + results['detected_communicating_samples'][0]['date'] 


    if 'detected_downloaded_samples' in results:
        if bool(results['detected_downloaded_samples']):
            res['s_srcd'] = str(len(results['detected_downloaded_samples'])) + " " + results['detected_downloaded_samples'][0]['date']


    if ('detected_urls' in results and len(results['detected_urls']) > 0):
        res['urls'] = str(len(results['detected_urls'])) + " " + results['detected_urls'][0]['scan_date']

    return res
                


def main():

    # read the file, count addresses

    with open(input_file) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if public_ip(row[addr_type]):
                addresses.append(row[addr_type])

    my_list = Counter(addresses)



    # set num_rec from optional vars, or to a length of the list if unspecified
    if args.records:
        num_rec = args.records
    else:
        num_rec = len(my_list)
    req_time = int(num_rec) * 15

    # if option -s was selected, print addresses and exit
    if args.show is True:
        to_print = dict(my_list.most_common(num_rec))
        for key, value in to_print.iteritems():
            print(key + "\t" + str(value))
        exit()


    print("\n### You selected " + str(num_rec) + " out of " + str(len(my_list)) + " " + addr_type.lower() + " records found in the input file.")
    print("### It will take " + str(req_time / 60) + ":" + str(req_time % 60) + " minutes to do it.") 
    print("### This is due to the 4 queries per minute limit on VT.")
    print("### Interrupt execution at any time with Ctrl+C, or ...")
    raw_input("### ... press Enter to continue.\n")



    # print header
    print("+" + "-" * 148 + "+")
    print("| {:^19}|{:^5}|{:^29}|{:^6} | {:^25} | {:^25} | {:^26}|".format("IP Address","Hits","AS Owner","pDNS","callbacks","sourced","urls"))
    print("+" + "-" * 148 + "+")

    # print real stuff and things
    for key, value in my_list.most_common(num_rec):
        get_info = get_ip_info(key)
        sleep(15)
        
        purty = parse_vt_results(get_info)
        print("| {:^19}|{:^5}|{:^29}|{:^6} | {:^25} | {:^25} | {:^26}|".format(key,value,purty['asn'],purty['pdns'],purty['s_callbck'],purty['s_srcd'],purty['urls']))


    print("+" + "-" * 148 + "+\n")
    print("### LEGEND")
    print("### Hits shows how many times this IP occurred in the inspected log file.")
    print("### AS Owner is name of the company owning the ASN where this IP belongs to.")
    print("### pDNS represents number of known passive DNS records tied to this IP address in VT.")
    print("### callbacks represents number of samples that contacted this IP address and the timestamp of the last seen sample." )
    print("### sourced represents number of samples downloaded from this address, and the timestamp of the last seen sample.")
    print("### urls represents total number of bad urls seen behind this IP address and the timestamp of the last seen bad url.\n")

if __name__ == "__main__":
    main()
