[TOC]

# IOC Hunter
 
This is an IOC Hunter, python script used to assist in iOC review and hunting for traffic logs exported as csv file from Palo Alto Networks firewall. 

*If you are looking for detailed instructions, please visit Wiki found at:* https://bitbucket.org/playopenminded/ioch/wiki/Home

## Pre-requirements: 

### Firewall side
- Export logs for the affected host from the PAN NGFW.
- Remember to increase lines of exported logs from default 64,535 to 999,999 or so, before export.
- Try to export the logs from BEFORE you've noticed security issue. Depending on the size and availability, try to have a week before; if that doesn't make sense or you don't have them - whatever you have is good.

### Virus Total
- create an account on Virus Total
- get your own API key (Profile > Settings > API Key)
- for ease of use, add that key to your .bash_profile (export APIKEY_VT="_your_key_here_"); script will read it and you won't have to provide it in the CLI

### CLI
* python 2.7 (nothing special there.) To check if you have python v2.7 do following:
```
python -V 2>&1 | grep 2.7 > /dev/null ; if [ $? -eq 0 ] ; then echo "Python 2.7 installed, check passed."; else echo "You don't have Python 2.7 installed." ; fi
```

## Installing the script
If you have git and use it, you can issue: 

```
git clone git@bitbucket.org:playopenminded/ioch.git
```

If not, you can easily copy only the script itself:
```
curl -LO https://bitbucket.org/playopenminded/ioch/raw/7284e82e0031af23b53658ad3df108960a230f97/ioch.py
```

Or manually copy the script from the bitbucket page: https://bitbucket.org/playopenminded/ioch/src/master/ioch.py

Once you copied that script, you need to allow it to run. Change into script directory and set permissions to allow executing:

````bash
$ cd ioch
ioch/ $ chmod +x ioch.py
````


### Installing permanently
If you want to use this script more often, besides setting up your .bash_profile to include API Key (described in [Wiki on VT](https://bitbucket.org/playopenminded/ioch/wiki/How%20to%20use%20an%20API%20Key%20from%20Virus%20Total) in details), for ease of use, once you downloaded the script, you can make a local link to it in your local bin directory, so that next time you run it, you don't have to be in the same directory where the script is - you can run it from anywhere. To do that, you need to know location of your local bin folder, and an exact location of the script.
My script is in my /home/luciano/ioch folder, and in my own bash path I have local bin folder: "/usr/local/bin"; to add link from there I would create in the bin folder a symbolic link to ioch script. Here is example of checking path, checking script directory, and creating a symbolic link:
```fish
ioch/ $ pwd
/home/luciano/ioch
ioch/ $ echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
ioch/ $ ln -s /home/luciano/ioch/ioch.py /usr/local/bin/ioch
ioch/ $ ls -l /usr/local/bin/ioch
lrwxrwxrwx 1 luciano luciano 34 Nov 28 11:02 /usr/local/bin/ioch -> /home/luciano/ioch/ioch.py
```

Now, whenever I want to use ioch scrypt, I don't have to go to it's folder, I can do it from anywhere, and additionally - I don't have to type "ioch.py" - I can call it only with "ioch".

Please note that my path is generally larger variable, I cleaned it up for brewity and clarity. Your path will prolly be similar. It is always good idea to use bin folder where there is a "local" in the path, or usr, or even both, such as "/usr/local/bin".

### Caveats and Bugs
- please note that non-commercial VT accounts have limit on lookups; 4 lookups a minute. That limit is hard coded into the script.
- when querying VT, script first filters out private IP scopes (10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16). Using -s switch includes private IP addresses as well.

## Running the script
- You have some logs to review. If you exported logs for your affected endpoint, than you probably exported logs where you searched for source address (your endpoint), and wish to review destination logs. You should use -f dst as a switch. Here are possible options to use:


```bash
~ $ ioch.py -h
usage: ioch.py [-h] -l LOG [-k KEY] -f {src,dst} [-r RECORDS] [-s]

IOC Hunter (ioch) is a script that retrieves IOCs from VT based on exported
PAN FW Traffic logs.

optional arguments:
  -h, --help            show this help message and exit
  -l LOG, --log LOG     path to traffic_logs.csv file that is to be parsed
  -k KEY, --key KEY     Specify your free public API key you already obtained
                        from VirusTotal.
  -f {src,dst}, --frm {src,dst}
                        Query VirusTotal for Source or Destination IPs from
                        log file.
  -r RECORDS, --records RECORDS
                        number of records for which you wish to retrieve IOCs.
  -s, --show            Do not lookup in VT, just print IPs found from the
                        logs and sorted for review.

```

## Bugs

Please report bugs to luciano (at) playopenminded (.com)
